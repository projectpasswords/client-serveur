
créer un utilisateur  UID(string username) coté client
=> envoi serveur UID
serveur il UID existe => message retour en erreur

si le UID est nouveau => serveur renvoie d'un random SEL au client


faire cote client
Récuperation du SEL
demande du password a l utilisateur connecté

- modification du password => 3 derivations (en première étape du projet)

  - uper  global
  - lower global
  - casse inversée premiere lettre
  
=> envoie les 3 chaines d'authentification dans un seul message  (UID(nom)  , (type_de_HASH), SEL, HASH(sel+password)
REM: on ecrit les HASH en caracteres hexadecimal

choix du HASH : sha256 ? a changer à la fin en SHA3 ?

Ensuite cote serveur, le serveur stocke les 3 chaines dans sa base de données utilisateurs


   =======
   
  coté client:
  scénario de tentative de login:
  UID(name) envoyé
  si UID existe le serveur renvoie le SEL
  utilisateur tape son mot de passe
  on derive 3 autres passwords
  le client ajoute SEL au 3 derivations et calcul les 3 HASH
  le client envoie les trois HASH en un seul message
  
  le serveur verifie la presence d'un des 3 HASH dans sa base
  reponse OK ou KO du serveur vers le client
  
  
  ======================
  format retenu pour la BDD  :    .csv ?
  
   ================
    
  ======================
  format retenu pour les messages:
  
  messe_id,contents
  
  
  
   ================ 
   
   AUTOMATE  CLIENT :
   
   
   * attendre entree clavier  USER_ID
      test si entree==EXIT
			=> FIN
      sinon
			envoi USER_ID au serveur
			attente reponse
			si sel OK ou nouveau SEL
				attente frappe clavier MOT DE PASSE
			
			
	* attente entree clavier MOT DE PASSE		
		test si entree==EXIT
			=> FIN
      sinon	
      	envoi hash(MOT DE PASSE + SEL) en fait [n fois x hash(password+sel)]  au serveur
			attente reponse authentification
			   afficher statut de la reponse: OK ou KO
			   attendre frappe clavier  USER_ID
      
    ================
   
   
   AUTOMATE  SERVEUR :     
   
      * attendre du message  USER_ID
			tant que:
			si reception 
				envoi reponse au client
							SEL (si USER_ID deja en base) 
							ou nouveau SEL (pour creation d'un nouveau USER_ID ) 
							=> next etat = attente message password
			fin tant que
   
      * attendre du message  PASSWORD
      	tant que:
				si reception: 
				teste si correct   => LOGIN OK  et  next etat = wait for USER_ID
						si incorrect => login KO  et  next etat = wait for PASSWORD
						si nouveau   => enregistrement et  next etat = USER_ID
						envoi reponse au client
			fin tant que
			  
		test si entree clavier==EXIT
		=> FIN


    =============================================================================================
    
   FONCTION CANONIQUE
   
   but: prendre en compte l'inversion de frappe main gauche main droite, dans le cas d'inversion de la casse :
   MAJ + Car1 + Car2    =>  Car1 + MAJ + Car2 


Cette fonction derive des password en permutant:
une touche de mise en majuscule (MAJ ou bien SHITF) avec la lettre 
suivante selon le schéma:  
   MAJ + Car1 + Car2    =>  Car1 + MAJ + Car2 
   
Cette fonction de dérivation s'applique en partant de la gauche sur le 
premier changement de casse rencontré   
Exemple :  
   passYann  => pasSyann
				=> passyAnn

Pour obtenir une fonction canonique facilement calculable on va considérer des blocs de trois lettres.
Le password initial est donc découpé en bloc de 3 lettres en partant de la gauche.

ex:

  "pAssyanncompliqué"  
  
  =>   bloc 1  :  pAs
  =>   bloc 2  :  sya  
  =>   bloc 3  :  nnc
  =>   bloc 4  :  omp
  =>   bloc 5  :  liq    
   etc ...
 
 
Bloc par bloc la fonction examine s'il y a une inversion de casse dans un des blocs.
Pour des raisons pratiques de cas usuels, on vise des mots de passe autour de 10 caractères.

L'indice du bloc sera retenu pour établir la forme canonique, du coup on dispose de 10 chiffres pour indicer.
Autant les utiliser : donc on va donc s'arrêter au 9eme bloc, cela fonctionnera utilement pour les password de 27 caractères.
On considérera que s'il n'y a pas d'inversion 
dans les 9 premiers blocs, on retiendra l'indice 0, 
la fonction pourra s'appliquer pour tous les password de longueurs quelconques.
On ne considére que le premier bloc avec inversion, en partant de la gauche.


La forme canonique à la forme :

    "indice, bloc1, ..., bloc K en minuscule (le premier ou il y a inversion en partant de la gauche) ,  bloc9, le reste des lettres"
    
    

 ALGO:
 
 si pas d inversion de casse:     
		forme canonique(  password ) ==     "0,password"
  
 si pas inversion de casse:    

		forme canonique(  pAssyanncompliqué )     ==     "1,passyanncompliqué"
		forme canonique(  passyanNcompliqué )     ==     "3,passyanncompliqué"
		forme canonique(  passyanncomPliqué )     ==     "4,passyanncompliqué"
		forme canonique(  PAssyanncomPliqué )     ==     "1,passyanncompliqué"
		forme canonique(  PASsyanncomPliqué )     ==     "0,PASsyanncomPliqué"
		
		forme canonique(  passyanncompliqué123456789012345675890AzAzAz90 ) 
					==      "0,passyanncompliqué123456789012345675890AzAzAz90"
		
rem:  la virgule permet de placer l'indice donc on pourrait ecrire des indices avec deux ou trois chiffres, 
		il n'y a pas vraiment d'interêt pour l'usage prévu d'un utilisateur qui tape à la main son password.


Dénombrement de la réduction:
	abc  => 0,abc
	ABC  => 0,abc
	
	Abc  => 1,abc
	ABc  => 1,abc
	Abc  => 1,abc
	AbC  => 1,abc
	abC  => 1,abc
	aBC  => 1,abc

	Il y a deux cas:
	  même casse  				2 vers 1
	  inversion de casse 	6 vers 1
	
	Cette forme canonique rassemble au plus 6 variantes d'un password vers une seule représentation.
	C'est donc à la fois intéressant:
		-  en terme de correction (de cas usuels d'erreur de frappe) 
		-  en terme de robustesse dans l'espace de tous les password
		
Rem: elle ne gère pas tous les cas ou d'erreur sur une transition entre deux blocs .
	Le  cas d'erreur suivant ne sera pas pris en compte:
	  (ex: si password initial == "abcDef"   => alors  "abCdef" ne sera pas accepté car différent de la représentation canonique), 
	mais ce n'est pas très grave, le but étant d'apporter un confort à l'utilisateur 
		mais pas de corriger tout les cas symétriques de variantes)
	
	
  ===========================================================================================================

