import socket
import ssl

port = 8082

CERT_PATH = "./Cert_SSL/"
KEY_CLIENT = 'client.key'
CERT_SERVER = 'server.pem'
CERT_CLIENT = 'client.pem'

while True:

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(10)

    # Require a certificate from the server. We used a self-signed certificate
    # so here ca_certs must be the server certificate itself.
    ssl_sock = ssl.wrap_socket(s, cert_reqs=ssl.CERT_REQUIRED, ca_certs=CERT_PATH + CERT_SERVER
                               , keyfile=CERT_PATH + KEY_CLIENT, certfile=CERT_PATH + CERT_CLIENT)

    ssl_sock.connect(('127.0.0.1', 8082))

    #ssl_sock.send("Hello World !".encode())
    response = ssl_sock.recv(1024)
    print(response.decode())

    ssl_sock.write(str(input("Enter Something: ")).encode())

    ssl_sock.close()





# import socket, ssl, pprint
#
# CERT_PATH = "./Cert_SSL/"
#
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
# # Require a certificate from the server. We used a self-signed certificate
# # so here ca_certs must be the server certificate itself.
# ssl_sock = ssl.wrap_socket(s,
#                            ca_certs=CERT_PATH + "server.crt",
#                            cert_reqs=ssl.CERT_REQUIRED)
#
# ssl_sock.connect(('localhost', 10000))
#
# print(repr(ssl_sock.getpeername()))
# print(ssl_sock.cipher())
# print(pprint.pformat(ssl_sock.getpeercert()))
#
# ssl_sock.write("boo!".encode())
#
# if False: # from the Python 2.7.3 docs
#     # Set a simple HTTP request -- use httplib in actual code.
#     ssl_sock.write("""GET / HTTP/1.0r
#     Host: www.verisign.comnn""")
#
#     # Read a chunk of data.  Will not necessarily
#     # read all the data returned by the server.
#     data = ssl_sock.read()
#     print("data : " + str(data))
#
#     # note that closing the SSLSocket will also close the underlying socket
#     ssl_sock.close()
