import socket
from socket import AF_INET, SOCK_STREAM, SO_REUSEADDR, SOL_SOCKET, SHUT_RDWR
import ssl

CERT_PATH = "./Cert_SSL/"
KEY_SERVER = 'server.key'
CERT_SERVER = 'server.pem'
CERT_CLIENT = 'client.pem'

def echo_client(s):
    while True:
        data = s.recv(8192)
        print(data.decode("utf-8"))
        if data == b'':
            break
        s.send(b'This is a response.')
        print('Connection closed')
    s.close()


def echo_server(address):
    s = socket.socket(AF_INET, SOCK_STREAM)
    s.bind(address)
    s.listen(1)
    s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

    s_ssl = ssl.wrap_socket(s, ca_certs=CERT_PATH + CERT_CLIENT, keyfile=CERT_PATH + KEY_SERVER
                            , certfile=CERT_PATH + CERT_SERVER, server_side=True)
    #cert = s_ssl.getpeercert()
    #print(cert)

    while True:
        try:
            (c, a) = s_ssl.accept()
            print('Got connection', c, a)
            echo_client(c)

        except socket.error as e:
            print('Error: {0}'.format(e))


echo_server((socket.gethostbyname('127.0.0.1'), 8082))




# import socket, ssl
#
# CERT_PATH = "./Cert_SSL/"
#
# bindsocket = socket.socket()
# bindsocket.bind(('', 10000))
# bindsocket.listen(5)
#
#
# def do_something(connstream, data):
#     print("do_something:", data)
#     return False
#
#
# def deal_with_client(connstream):
#     data = connstream.read()
#     print(" connstream : " + str(connstream))
#     print("data : " + str(data))
#     while data:
#         if not do_something(connstream, data):
#             break
#         data = connstream.read()
#         print("data : " + str(data))
#
#
# while True:
#     newsocket, fromaddr = bindsocket.accept()
#     connstream = ssl.wrap_socket(newsocket,
#                                  server_side=True,
#                                  certfile=CERT_PATH + "server.crt",
#                                  keyfile=CERT_PATH + "server.key")
#     try:
#         deal_with_client(connstream)
#     finally:
#         connstream.shutdown(socket.SHUT_RDWR)
#         connstream.close()