#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import sys
import outils
import ssl

from messages_id import *

###################################

###################################


CLIENT_WAIT_USER = 1
CLIENT_WAIT_PASSWORD = 2
CLIENT_WAIT_AUTHENTIFICATION = 3
CLIENT_WAIT_INSCRIPTION = 4

CERT_PATH = "./Cert_SSL/"
KEY_CLIENT = 'client.key'
CERT_SERVER = 'server.pem'
CERT_CLIENT = 'client.pem'

###################################


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
port = 10000
ip_adr = 'localhost'
print('starting up on {} port {}'.format(ip_adr, port))

s_ssl = ssl.wrap_socket(sock, cert_reqs=ssl.CERT_REQUIRED, ca_certs=CERT_PATH + CERT_SERVER
                        , keyfile=CERT_PATH + KEY_CLIENT, certfile=CERT_PATH + CERT_CLIENT)

server_address = (ip_adr, port)
s_ssl.connect(server_address)


###################################


def attente_reponse():
    # attente reponse
    amount_received = 0
    amount_expected = MAX_BUF_SIZE

    if amount_received < amount_expected:
        response = s_ssl.recv(MAX_BUF_SIZE).decode()
        amount_received += len(response)
        print("received ", response)

    return response


###################################"

def automate():
    # etat: attente frappe clavier
    # etat: attente reponse du serveur
    # etat: fabrication des derivés de password
    # etat : envoi message au serveur

    current_state = CLIENT_WAIT_USER
    entree = input("username :")
    # entree = SIGN_IN_USER   # => pour forcer le USER

    run = True
    if entree == "q":  # quit
        run = False

    while run:

        if current_state == CLIENT_WAIT_USER:
            message = MESS_USER + "," + entree
            s_ssl.sendall(message.encode())  # envoie du username au serveur
            next_state = CLIENT_WAIT_USER
            automate.create_sel = False
            response = attente_reponse()

            if len(response) > 3:
                # analyse reponse
                mess_id = response.split(',')[0]
                user = response.split(',')[1]
                automate.user = user

                if mess_id == MESS_REQ_PASS:
                    print('MESS_REQ_PASS recu')
                    next_state = CLIENT_WAIT_PASSWORD
                elif mess_id == MESS_REQ_SEL_AND_PASS:
                    print('MESS_REQ_SEL_AND_PASS recu')
                    next_state = CLIENT_WAIT_PASSWORD
                    automate.create_sel = True
                else:
                    print('BAD MESSAGE recu')

        elif current_state == CLIENT_WAIT_PASSWORD:
            if automate.create_sel == True:
                automate.sel = outils.createSel()
                automate.userfile = outils.create_user_file(automate.user, automate.sel)
                # ajouter ici le stockage (user+sel) dans un fichier coté client
                hash_pass_derives = outils.create_pass_derives(entree, automate.sel)  # hash(pasword + sel)
                message = MESS_SEL_AND_PASS + "," + automate.user + "," + automate.sel + "," + hash_pass_derives
                s_ssl.sendall(message.encode())
                next_state = CLIENT_WAIT_INSCRIPTION
            else:
                automate.sel = outils.getSel(automate.user)  # a recuperer (user+sel) dans un fichier coté client
                #hash_pass = outils.connect_user(automate.user, entree, automate.sel)  # hash(pasword + sel)
                hash_pass = outils.create_pass_derives(entree, automate.sel) 
                message = MESS_PASSWORD + "," + automate.user + ",sel," + hash_pass  # "sel" dans champs sel pour uniformite traitement message coté serveur
                s_ssl.sendall(message.encode())
                next_state = CLIENT_WAIT_AUTHENTIFICATION

        elif current_state == CLIENT_WAIT_INSCRIPTION:
            response = attente_reponse()
            if len(response) > 3:
                inscription = response.split(',')[0]
            if inscription == MESS_OK:
                print('INSCRIPTION   OK')
            else:
                print('INSCRIPTION   FAIL')
            next_state = CLIENT_WAIT_USER

        elif current_state == CLIENT_WAIT_AUTHENTIFICATION:
            response = attente_reponse()
            if len(response) > 3:
                authen = response.split(',')[0]
            if authen == MESS_OK:
                print('AUTHENTIFICATION   OK')
            else:
                print('AUTHENTIFICATION   FAIL')
            next_state = CLIENT_WAIT_USER

        else:
            run = False

        current_state = next_state
        entree == " "
        if current_state == CLIENT_WAIT_USER:
            entree = input("username:")

        if current_state == CLIENT_WAIT_PASSWORD:
            entree = input("password:")

        if entree == "q":
            run = False
        else:
            run = True


###################################"

def run_client():
    try:
        automate()

    finally:
        print('closing socket')
        s_ssl.close()


###################################"
run_client()
