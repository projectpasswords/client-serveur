#!/usr/bin/python3
# -*- coding: utf-8 -*-

import csv
import random
import hashlib
from pass_derivation import *
from pass_canonique import *
from fonct_canonique import *

############################################


FILE_BASE_PWD = "data.csv"
PATH_USERS_FILES = "./Users_files/" 

############################################
# revoir les fct => close()

def check_user(user):
	with open(FILE_BASE_PWD, "r") as data_base:
		csv.QUOTE_NONE
		reader = csv.reader(data_base, delimiter=',')
		for line in reader:
			if user == line[0]:
				print("User ", user, " exists")
				return True
				
	print("User ", user, " does not exist")
	return False

############################################
# recherche du password d'un username dans la bdd
# en entrée password est la chaine de tous les derivés sous la forme:  has1,hash2,hash3

def check_password(user, password):
    with open(FILE_BASE_PWD, "r") as data_pass:
        csv.QUOTE_NONE
        reader = csv.reader(data_pass, delimiter=',')
        for line in reader:
            if user == line[0]:
                del line[0]  # remove user
                del line[0]  # now, remove sel
                print(line)
                chaine_hashpassword = ','.join(line)
                # print(password)
                # print(chaine_hashpassword)

                liste_hash = password.split(',')
                for un_hash in liste_hash:
                    if (un_hash in chaine_hashpassword):
                        print("hash_password OK pour : ", user)
                        return True

    print("hash_password ERRONE pour :  : ", user)
    return False


############################################


def getSel(user):
    user_file = PATH_USERS_FILES + user + ".csv"

    with open(user_file, "r") as sel_file:
        for line in sel_file:
            sel = line.split(",")[0]
            print("Sel de " + user + " : " + sel)
            break
    return sel


############################################


def createSel():
    salt_alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    char = []
    for i in range(30):
        char.append(random.choice(salt_alphabet))
        new_salt = "".join(char)
    print("Nouveau sel : ", new_salt)
    return new_salt


############################################

def create_pass_derives(password, sel):
	# appel des fonctions canoniques:

	passint   = typosAtPositionOne(password)
	password1 = formecanoniquelowerUpper(passint)
	password2 = typosAtAllPositions(password)
	 
	password3 = fct_canonique_permutations_maj_min(password)

	print(password1, password2, password3)
	
	hash_pass1 = hashlib.sha256(password1.encode() + sel.encode()).hexdigest()
	hash_pass2 = hashlib.sha256(password2.encode() + sel.encode()).hexdigest()
	hash_pass3 = hashlib.sha256(password3.encode() + sel.encode()).hexdigest()
	
	print("Sel utilisé : ", sel)

	print("Hash de password1 : ", hash_pass1)
	print("Hash de password2 : ", hash_pass2)
	print("Hash de password3 : ", hash_pass3)
	
	pass_derives = hash_pass1 + "," + hash_pass2 + "," + hash_pass3  
	return pass_derives


############################################


def connect_user(user, password, sel):
    print("User : ", user)
    print("Password : ", password)
    print("Sel utilisé : ", sel)

    hash_pass = hashlib.sha256(password.encode() + sel.encode()).hexdigest()

    print("Hash de (sel+password) : ", hash_pass)
    return hash_pass


############################################


def record_new_user(user, sel, hash_pass):
    print("record_new_user(user, sel, hashpass)")
    print("\t\t user : ", user)
    print("\t\t Sel  : ", sel)
    print("\t\t Hash de password : ", hash_pass)

    base_pwd = open(FILE_BASE_PWD, "a")
    base_pwd.write(user + "," + sel + "," + hash_pass + "\n")
    base_pwd.close()
    return


############################################


def create_user_file(user, sel):
    user_file = PATH_USERS_FILES + user + ".csv"
    with open(user_file, 'w') as myfile:
        myfile.write(sel + ",")
    return sel


############################################
#   TESTS

def test():
    user = "matthieu"
    sel = "1234"

    check_user(user)
    
    createSel()

    #getSel(user)

    createSel()

    create_pass_derives("toto", sel)
    
    # connect_user("matthieu", "toto", sel)

    #firstLetterChange("Toto")

    #create_user_file(user, sel)

    print("##################################################")


#test()

