#!/usr/bin/python3
# -*- coding: utf-8 -*-





#########################################################################################
#
# fonction canonique:  (inversion de casse sur la mauvaise lettre)
#
# but: prendre en compte l'inversion de frappe main gauche main droite, dans le cas d'inversion de la casse :
#
# reduit un password à sa représentation canonique en permutant:
# une touche de mise en majuscule (MAJ ou bien SHITF) avec la lettre 
# suivante selon le schéma:  
# MAJ + Car1 + Car2    =>  Car1 + MAJ + Car2 
# Cette fonction canonique s'applique en partant de la gauche sur le 
# premier changement de casse rencontré   
#
# Pour obtenir une fonction canonique facilement calculable on va considérer des blocs de trois lettres.
# Le password initial est donc découpé en bloc de 3 lettres en partant de la gauche.
# 
# Exemple :  
#    passYann  => 1,passyann
#    passyAnN	=> 2,passyanN
#    passyann  => 0,passyann
#
#########################################################################################



def fct_canonique_permutations_maj_min(password):
	pass_length = len(password)
	print("\n\n===== debut fct_canonique_permutations_maj_min() =====")
	print("password = ", password, "    length = ", pass_length)
	
	liste_car = list(password)
	i = 0  # indice de la lettre dans la boucle for
	block = 0
	for car in list(password):
		# jusqu'a la premiere inversion de la casse (et seulement celle ci)
		# mais uniquement à l'intérieur d'un bloc de 3 caracteres
		# l'indice i partant de 0 si (i%3) ==  [0,1,2]
		if i%3 == 0 :
			# debut de block => reinit de la casse initiale
			if car.islower() == True:
				debut_min = True
			else:
				debut_min = False
			
		if car.isalpha() and ( car.islower() != debut_min ):
			block = i//3 + 1  # memorise le no de block ou se produit l'inversion de casse
									# le block 0 est reservé
			#print("diff casse indice =",i, "block =",block, " , caractere = ",car)
			break; # on arrete apres la premiere inversion de la casse
			
		i = i +1  # FIN for  => indice pour boucle for suivante
		if i>=27:
			break # on s'arrête au 9eme block au maximum soit 27 premiers caracteres
		
	# transformation en chaines :
	# en representation canonique
	# mise en minuscule des lettre du bloc
	if block > 0 :
		indice = (block-1) * 3
		liste_car[indice]   = liste_car[indice].lower()
		liste_car[indice+1] = liste_car[indice+1].lower()
		liste_car[indice+2] = liste_car[indice+2].lower()

	chaine_canonique = "".join(liste_car)
	chaine_canonique = str(block) + "," + "".join(liste_car)
		
	print("chaine_canonique  = ",chaine_canonique);
	return chaine_canonique


###############################################################
#
#  fonctions de tests et de verification de non regression
#
###############################################################


########################

def test_fct_canonique_permutations_maj_min():
	print("\n\n++++++ DEBUT test_fct_canonique_permutations_maj_min() ++++++\n\n")
		
	chaine_A = fct_canonique_permutations_maj_min("paBcef")
	chaine_B = fct_canonique_permutations_maj_min("pabCef")
	chaine_C = fct_canonique_permutations_maj_min("pab123Cef")
	chaine_D = fct_canonique_permutations_maj_min("pabcEf")
	chaine_E = fct_canonique_permutations_maj_min("pabcEfNjk")
	chaine_F = fct_canonique_permutations_maj_min("pabcefAAABBBDDDEEE12345678901234567890Njk")
	chaine_G = fct_canonique_permutations_maj_min("pabKLMklm")
	chaine_H = fct_canonique_permutations_maj_min("pa")
	chaine_I = fct_canonique_permutations_maj_min("pA34")
	chaine_J = fct_canonique_permutations_maj_min("pa34")	
	
	
	print("\n\n++++++ FIN test_fct_canonique_permutations_maj_min() ++++++\n\n")

########################

########################

###################################
	

test_fct_canonique_permutations_maj_min()
###################################





