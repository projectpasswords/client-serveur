#!/usr/bin/python3
# -*- coding: utf-8 -*-



#########################################################################################
#  inverse la casse d'un caractère
# 
def inv_casse(carac):
	carac_low  = carac.lower()
	carac_up   = carac.upper()
	
	if carac == carac_up :
		carac = carac_low
	else:
		carac = carac_up
	
	return carac


#########################################################################################
#
# Cette fonction derive des password en permutant:
# une touche de mise en majuscule (MAJ ou bien SHITF) avec la lettre 
# suivante selon le schéma:  
#  MAJ + C1 + C2    =>  C1 + MAJ + C2 
# Cette fonction de dérivation s'applique en partant de la gauche sur le 
# premier changement de casse rencontré   
# Exemple :  
#    passYann  => pasSyann
#					=> passyAnn
#
# il faut donc garder trois empreintes de hash (pour: pasSyann , passYann ,passyAnn)
#
#########################################################################################



def derive_permutations_paire_maj_min(password):
	tab_password = [""] 
	tab_password *= 3 # duplication du 1er element 3 fois
	pass_length = len(password)
	print("\n\n===== debut =====")
	print("password = ", password, "    length = ", pass_length)

	# les trois derivations par defaut:
	tab_password[0] = list(password)   # copie de tous les caracteres
	tab_password[1] = list(password)
	tab_password[2] = list(password)
	
	if password[0].islower() == True:
		casse_debut = "min"
		debut_min = True
	else:
		casse_debut = "MAJ"
		debut_min = False
		
	print(casse_debut)


	i = 0  # indice de la lettre dans la boucle for
	for car in list(password):
		# jusqu'a la premiere inversion de la casse (et seulement celle ci)
		if car.isalpha() and (car.islower() != debut_min):
			print("diff casse indice =",i, " , caractere = ",car, " , islower=",car.islower())
			# on genere une chaine en permutant la casse sur deux lettres successives
			
			#------ premiere derivation, placée dans => tab_password[0] ------
			# uniquement si i ne correspond pas a la derniere lettre
			liste_car       = tab_password[0]
			liste_car[i]    = inv_casse(password[i])
			if i < (pass_length -1):
				liste_car[i+1]  = inv_casse(password[i+1])
			tab_password[0] = liste_car
			print(i,"       ", tab_password[0] )
			
			#------ deuxieme derivation, placée dans => tab_password[1] -------
			
			if i > 0:  # rem: en fait 'car' n'est jamais la premiere lettre
				liste_car       = tab_password[1]
				liste_car[i-1]  = inv_casse(password[i-1])
				liste_car[i]    = inv_casse(password[i])
				tab_password[1] = liste_car
				print(i,"       ", tab_password[1] )
				
			break; # on arrete apres la premiere inversion de la casse		
		i = i +1  # FIN for  => indice pour boucle for suivante

	# transformation en chaines :
	liste_tous_password=[]
	for i in range(0, 3):
		liste_tous_password.append("".join(tab_password[i]) )
	
	liste_tous_password.sort()  # sert surtout au debug, car une fois hasché il n'y a plus d'ordre ...
	print(liste_tous_password)
	return liste_tous_password


######################################################################################
#
# Cette fonction derive des password en permutant:
# un caractère avec le caractère suivant
#  
#  selon le schéma:  
#
#   C1 C2 C3 C4 C5 ...   =>    C2 C1 C3 C4 C5 ...
#   C1 C2 C3 C4 C5 ...   =>    C1 C3 C2 C4 C5 ...
#   C1 C2 C3 C4 C5 ...   =>    C1 C2 C4 C3 C5 ...
#
# Cette fonction de dérivation s'applique en partant de la gauche 
# sur au maximum les 7 premieres permutations de paires
# la 8eme "derivation" est fait le password lui même, il faut evidement le garder
#
# Exemple :  
#    passYann  => apssYann
#					=> psasYAnn
#					=> passYAnn
#					=> pasYsAnn
#              =>  etc ...
#
#
#  si le password fait moins de 9 caractères, les dernières empreintes sont dupliquées
#  il faut en effet garder le bon nombre de colonne dans la base serveur )
#
#   il faut donc garder huit variantes de password pour 8 empreintes de hash 
#   elle sont classées dans l'ordre alphabétique
#	 on aura donc 8 HASH enregistrés sur le serveur
# 
#########################################################################################


def derive_permutations_par_paire(password):
	NB_VARIANTE = 8
	tab_password = [""] 
	pass_length = len(password)
	print("\n\n===== debut =====")
	print("password = ", password, "    length = ", pass_length)


	tab_password[0] = password   	# copie de tous les caracteres
	tab_password *= NB_VARIANTE	# allocation dynamique par : duplication du 1er element 8 fois
											# comme cela si la chaine fais moins de 8 caraacteres, 
											# on aura toujours les 8 chaines en sorties
	print(tab_password)	

	nb_permutation = min(NB_VARIANTE,pass_length)  # huit inversion au maximum
	for i in range(0, nb_permutation-1):
		tab_password[i] = list(password)   # copie de tous les caracteres
		temp = password[i]
		liste_car = tab_password[i]
		liste_car[i]    = password[i+1]
		liste_car[i+1]  = temp
		tab_password[i] = liste_car
		print(tab_password[i])
			
	# ajout du password initial
	tab_password[nb_permutation-1] = list(password)
	
	# classement alphabetique des derives
	liste_tous_password=[]
	for i in range(0, NB_VARIANTE):
		liste_tous_password.append("".join(tab_password[i]) )
	
	liste_tous_password.sort()
	print(liste_tous_password)
	
	#concatenation
	#chaine_finale ="".join(liste_tous_password)

	print("===== FIN =====")
	return liste_tous_password



###############################################################
#
#  fonctions de tests et de verification de non regression
#
###############################################################

def check_intersection(listA , listB):
	setA = set(listA)
	setB = set(listB)
	setC = setA.intersection(setB)
	if len(setC)== 0 :
		print("\n    ERROR REGRESSION")
	else:
		print("\n    intersection = ",setC, " OK ")

########################

	
def test_derive_permutations_paire_maj_min():
	
	print("\n\n++++++ DEBUT test_derive_permutations_paire_maj_min() ++++++\n\n")
	
	derive_permutations_paire_maj_min("A")
	derive_permutations_paire_maj_min("aZ")
	derive_permutations_paire_maj_min("aZb")
	derive_permutations_paire_maj_min("aZB")
	derive_permutations_paire_maj_min("abZ")
	derive_permutations_paire_maj_min("rPabcef")
	
	liste_A = derive_permutations_paire_maj_min("paBcef")
	liste_B = derive_permutations_paire_maj_min("pabCef")
	check_intersection(liste_A , liste_B)
	
	print("\n\n++++++ FIN test_derive_permutations_paire_maj_min() ++++++\n\n")

########################

def test_pass_derivation():
	
	print("\n\n++++++ DEBUT test_pass_derivation() ++++++\n\n")
	
	derive_permutations_par_paire("A")
	
	liste_A = derive_permutations_par_paire("AB")
	liste_B = derive_permutations_par_paire("BA")
	check_intersection(liste_A , liste_B)
		
	liste_A = derive_permutations_par_paire("ABC")
	liste_B = derive_permutations_par_paire("ACB")
	check_intersection(liste_A , liste_B)
	
	liste_A = derive_permutations_par_paire("pas45678")
	liste_B = derive_permutations_par_paire("pas54678")
	check_intersection(liste_A , liste_B)
			
	liste_A = derive_permutations_par_paire("pas4567890")
	liste_B = derive_permutations_par_paire("pas5467890")
	check_intersection(liste_A , liste_B)
		
	print("\n\n++++++ FIN test_pass_derivation() ++++++\n\n")
	
###################################
	

test_derive_permutations_paire_maj_min()
test_pass_derivation()

###################################





