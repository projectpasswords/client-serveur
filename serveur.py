#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import sys
import outils
import ssl

from messages_id import *

############################################

STATE_WAIT_USER = 1
STATE_SEND_SEL = 2
STATE_SEND_FAILURE = 3
STATE_END = 4
STATE_WAIT_PASSWORD = 5

CERT_PATH = "./Cert_SSL/"
KEY_SERVER = 'server.key'
CERT_SERVER = 'server.pem'
CERT_CLIENT = 'client.pem'

############################################

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
port = 10000
ip_adr = 'localhost'
print('starting up on {} port {}'.format(ip_adr, port))

server_address = (ip_adr, port)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

# Encapsulation SSL de la socket
s_ssl = ssl.wrap_socket(sock, ca_certs=CERT_PATH + CERT_SERVER, keyfile=CERT_PATH + KEY_SERVER
                        , certfile=CERT_PATH + CERT_SERVER, server_side=True)

# Wait for a connection
print('waiting for a connection')
connection, client_address = s_ssl.accept()
print('connection from', client_address)


############################################


def automate(current_state):
    next_state = STATE_END
    return_message = MESS_KO + "," + "0"

    # ----------------------------------#
    if current_state == STATE_WAIT_USER:

        data = connection.recv(MAX_BUF_SIZE).decode()
        print('message recu : {} '.format(data))
        mess_id = data.split(',')[0]
        user = data.split(',')[1]

        if mess_id == MESS_USER:
            print('recu MESS_USER')
            print('user = ', user)
            automate.last_user = user

            if outils.check_user(user) == True:
                automate.b_new = False  # user existe dejà
                return_message = MESS_REQ_PASS + "," + user
                print('return MESS_REQ_PASS = ', user)
                connection.sendall(return_message.encode())
                next_state = STATE_WAIT_PASSWORD
            else:
                automate.b_new = True  # user n 'existe pas il va être crée
                # après reception du password
                # automate.sel = outils.createSel() # generation d un SEL aléatoire
                return_message = MESS_REQ_SEL_AND_PASS + "," + user
                print('return MESS_REQ_SEL_AND_PASS = ', user)
                connection.sendall(return_message.encode())
                next_state = STATE_WAIT_PASSWORD

        else:
            current_state = STATE_END
            next_state = STATE_END

    # ----------------------------------#
    elif current_state == STATE_WAIT_PASSWORD:

        data = connection.recv(MAX_BUF_SIZE).decode()
        print('message recu : {} '.format(data))
        if len(data) > 5:
            mess_id = data.split(',')[0]
            user = data.split(',')[1]
            sel = data.split(',')[2]
            liste_champs = data.split(',')
            del liste_champs[0:3]  # on garde:   hash1,hash2,hash3,...
            password = ",".join(liste_champs)
        # print(password)

        else:
            print('bad message')
            mess_id = MESS_PB
            user = ""
            sel = ""
            password = ""
            return_message = MESS_KO + "," + automate.last_user
            ext_state = STATE_WAIT_PASSWORD

        if mess_id == MESS_PASSWORD:  # user existe deja
            print('recu MESS_PASSWORD')
            print('password = ', password)

            retour = outils.check_password(automate.last_user, password)
            if retour == True:
                print('password OK au client...')
                return_message = MESS_OK + "," + automate.last_user
                next_state = STATE_WAIT_USER

            else:
                # user existe mais le control du password n est pas bon
                print('password NOT OK au client...')
                return_message = MESS_KO + "," + automate.last_user
                next_state = STATE_WAIT_PASSWORD  # en attente d'un autre essai


        elif mess_id == MESS_SEL_AND_PASS:  # user n 'existe pas il va être crée
            print('recu MESS_SEL_AND_PASS')
            print('les hash_password = ', password)
            if automate.b_new == True:
                #  enregistrement en base  [ user, sel, hash(sel+password) ]
                outils.record_new_user(user, sel, password)

                print('ENROLLEMENT (user + password) OK => au client...')
                return_message = MESS_OK + "," + user
                automate.b_new = False
                automate.last_user = user
                next_state = STATE_WAIT_USER

        else:
            print('bad message')
            return_message = MESS_KO + "," + automate.last_user
            next_state = STATE_WAIT_PASSWORD

        connection.sendall(return_message.encode())

    # ----------------------------------#

    else:
        current_state = STATE_END  # provoquera l'arrêt de l'automate
        next_state = STATE_END
    # ----------#

    return next_state


############################################


def run_serveur():
    if True:
        try:
            # Receive the data in small chunks and retransmit it
            current_state = STATE_WAIT_USER

            while current_state != STATE_END:
                next_state = automate(current_state)
                current_state = next_state

                if (current_state == STATE_WAIT_USER):
                    entree = input("quit ?")
                    if (entree == "q"):
                        current_state = STATE_END

        finally:
            # Clean up the connection
            connection.close()


###################################

run_serveur()
